<?php namespace Tests\PayPal;

use Framework\PayPal\Config;
use Framework\PayPal\Payment;
use Framework\Shop\Product;
use PHPUnit\Framework\TestCase;

class PaymentTest extends TestCase
{
	protected Payment $payment;

	public function setup() : void
	{
		$config = new Config();
		$config->environment = 'sandbox';
		$config->clientId = 'AR7HMHf18xJ9ynYU7qsXEbnNlznnvUcNy3lRHpiNy2KQ3_V-U0OTlBWtYBDTLvRp2JufV-LMUwR64ll_';
		$config->clientSecret = 'EJoytQZ8gXCRuWK6z1QJ6L8nSqoOGQVa0OW-dx5LLetX9NyIYU247H1hV6cA2xN7NHeMf3Rc3-6p8yGF';
		$this->payment = new Payment($config);
	}

	public function testRegister()
	{
		//$this->payment->setCustomer('John Doe', 'jd@mail.com');

		$item1 = new Product();
		$item1->id = 'ABC123';
		$item1->title = 'Monitor LG';
		$item1->price = '400.00';

		$item2 = new Product();
		$item2->id = 'XYZ';
		$item2->title = 'Teclado Positivo';
		$item2->price = 40.00;

		$this->payment->setOrder([
			$item1,
			$item2,
		], 'ABC123');
		$link = $this->payment->getLink();
		$this->assertStringStartsWith(
			'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=',
			$link
		);
	}
}
