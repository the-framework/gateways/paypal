# Framework PayPal Gateway Package

- [Homepage](https://gitlab.com/the-framework/gateways/paypal)
- [API Documentation](https://the-framework.gitlab.io/gateways/paypal/docs/)

[![Build](https://gitlab.com/the-framework/gateways/paypal/badges/master/pipeline.svg)](https://gitlab.com/the-framework/gateways/paypal/-/jobs)
[![Coverage](https://gitlab.com/the-framework/gateways/paypal/badges/master/coverage.svg?job=test:php)](https://the-framework.gitlab.io/gateways/paypal/coverage/)
