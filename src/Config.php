<?php namespace Framework\PayPal;

class Config
{
	public string $environment = 'sandbox';
	public string $clientId;
	public string $clientSecret;
	public string $currency = 'BRL';
	public string $returnURL = 'http://localhost:8080/success';
	public string $cancelURL = 'http://localhost:8080/cancel';
	public bool $logActive = false;
	public string $logPath = __DIR__ . '/../logs/paypal.log';
}
