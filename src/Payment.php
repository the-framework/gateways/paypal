<?php namespace Framework\PayPal;

use Framework\Shop\PaymentInterface;
use Framework\Shop\Product;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment as PayPalPayment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

class Payment implements PaymentInterface
{
	protected Config $config;
	protected ApiContext $apiContext;
	protected Transaction $transaction;

	public function __construct(Config $config)
	{
		$this->config = $config;
		$this->apiContext = $this->getApiContext(
			$config->clientId,
			$config->clientSecret,
			$config->environment === 'sandbox'
		);
	}

	protected function getApiContext(
		string $clientId,
		string $clientSecret,
		bool $sandbox = false
	) : ApiContext {
		$apiContext = new ApiContext(
			new OAuthTokenCredential(
				$clientId,
				$clientSecret
			)
		);

		$apiContext->setConfig(
			[
				'mode' => $sandbox ? 'sandbox' : 'live',
				'log.LogEnabled' => $this->config->logActive,
				'log.FileName' => $this->config->logPath,
				'log.LogLevel' => $this->config->environment === 'sandbox' ? 'DEBUG' : 'INFO',
				// PLEASE USE `INFO` LEVEL FOR LOGGING IN LIVE ENVIRONMENTS
				'cache.enabled' => true,
				//'cache.FileName' => '/PaypalCache' // for determining paypal cache directory
				// 'http.CURLOPT_CONNECTTIMEOUT' => 30
				// 'http.headers.PayPal-Partner-Attribution-Id' => '123123123'
				//'log.AdapterFactory' => '\PayPal\Log\DefaultLogFactory' // Factory class implementing \PayPal\Log\PayPalLogFactory
			]
		);

		return $apiContext;
	}

	/**
	 * @param array|Product[] $products
	 * @param string|null     $invoice_number
	 * @param float           $shipping
	 */
	public function setOrder(array $products, string $invoice_number = null, float $shipping = 0)
	{
		$items = [];
		foreach ($products as $key => $product) {
			$items[$key] = new Item();
			$items[$key]->setName($product->title)
				->setPrice($product->price)
				->setQuantity($product->quantity)
				->setCurrency($this->config->currency);
		}
		$itemList = new ItemList();
		$itemList->setItems($items);

		$subtotal = 0;
		foreach ($items as $item) {
			$subtotal += $item->getQuantity() * $item->getPrice();
		}

		$details = new \PayPal\Api\Details();
		$details->setShipping($shipping)
			// ->setTax($tax)
			->setSubtotal($subtotal);
		$amount = new \PayPal\Api\Amount();
		$amount->setCurrency($this->config->currency)
			->setTotal($subtotal + $shipping /*+ $tax*/)
			->setDetails($details);

		$this->transaction = new Transaction();
		$this->transaction->setAmount($amount)
			->setItemList($itemList)
			//->setDescription("Payment description")
			->setInvoiceNumber($invoice_number);
	}

	public function getLink() : string
	{
		$payer = new Payer();
		$payer->setPaymentMethod('paypal');

		$redirectUrls = new RedirectUrls();
		$redirectUrls->setReturnUrl($this->config->returnURL)
			->setCancelUrl($this->config->cancelURL);

		$payment = new PayPalPayment();
		$payment->setIntent('order')
			->setPayer($payer)
			->setRedirectUrls($redirectUrls)
			->setTransactions([$this->transaction]);
		$payment->create($this->apiContext);
		return $payment->getApprovalLink();
	}
}
